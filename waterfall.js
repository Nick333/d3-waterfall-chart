(function (w) {
    const INCREASE = 'inc';
    const DECREASE = 'dec';
    const MARGIN = {
        TOP: 20,
        RIGHT: 20,
        BOTTOM: 60,
        LEFT: 60
    };
    const POSITIVE_CLASS_NAME = 'positive';
    const NEGATIVE_CLASS_NAME = 'negative';
    const TOTAL_CLASS_NAME = 'total';

    const FULL_WIDTH = document.documentElement.clientWidth;
    const FULL_HEIGHT = document.documentElement.clientHeight;
    const HEIGHT = FULL_HEIGHT - MARGIN.TOP - MARGIN.BOTTOM;
    const WIDTH = FULL_WIDTH - MARGIN.LEFT - MARGIN.RIGHT;

    function copyObject(object) {
        let newObject = {};
        Object.keys(object).forEach(key => {
            if (typeof object[key] == 'object' && object[key].length === 0) {
                newObject[key] = copyObject(object[key]);
            } else if (typeof object[key] == 'object' && object[key].length !== 0) {
                newObject[key] = object[key].map(el => {
                    return copyObject(el);
                })
            }
            else {
                newObject[key] = object[key];
            }
        });
        return newObject;
    }


    function mapper(data, totalResult) {
        return data.map((el) => {
            let obj = copyObject(el);
            obj.start = totalResult;
            if (obj.dir === INCREASE) {
                totalResult += obj.value
            } else {
                totalResult -= obj.value
            }
            obj.end = totalResult;
            obj.class = obj.dir === INCREASE ? POSITIVE_CLASS_NAME : NEGATIVE_CLASS_NAME;
            if (obj.embedData && obj.embedData.length !== 0) {
                let newtotalResult = obj.start;
                obj.embedData = mapper(obj.embedData, newtotalResult);
            }
            return obj;
        });
    }

    function dataBuilder(data) {
        let totalResult = 0;
        return mapper(data, totalResult);
    }

    w.WaterfallChart = function (container, data) {
        this.container = container;
        this.data = data;
    };

    WaterfallChart.prototype.init = function () {
        const container = this.container;
        const svg = d3.select(container)
            .append('svg')
            .attr('height', FULL_HEIGHT)
            .style('background-color', '#fff')
            .attr('width', FULL_WIDTH)
            .call(responsivefy)
            .append('g')
            .attr('transform', 'translate(' + MARGIN.LEFT + ', ' + MARGIN.TOP + ')')
            .style('fill', 'red');


        function responsivefy(svg) {
            // get container + svg aspect ratio
            var container = d3.select(svg.node().parentNode),
                width = parseInt(svg.style('width')),
                height = parseInt(svg.style('height')),
                aspect = width / height;

            // add viewBox and preserveAspectRatio properties,
            // and call resize so that svg resizes on inital page load
            svg.attr('viewBox', '0 0 ' + width + ' ' + height)
                .attr('preserveAspectRatio', 'xMinYMid')
                .call(resize);

            // to register multiple listeners for same event type,
            // you need to add namespace, i.e., 'click.foo'
            // necessary if you call invoke this function for multiple svgs
            // api docs: https://github.com/mbostock/d3/wiki/Selections#on
            d3.select(window).on('resize.' + container.attr('id'), resize);

            // get width of container and resize svg to fit it
            function resize() {
                var targetWidth = parseInt(container.style('width'));
                svg.attr('width', targetWidth);
                svg.attr('height', Math.round(targetWidth / aspect));
            }
        }

        let toolTip = d3.select(container)
            .append('div')
            .attr('id', 'toolTip')
            .style('position', 'fixed');

        let maxValue = this.data.reduce((sum, current) => {
            if (current.dir === INCREASE) {
                return sum + current.value
            }
            return sum;
        }, 0);

        let data = dataBuilder(this.data);

        let totalResult = data.reduce((sum, el) => {
            if (el.dir === INCREASE) {
                return sum + el.value;
            } else {
                return sum - el.value;
            }
        }, 0);


        data.push({
            label: 'Total',
            value: totalResult,
            start: 0,
            end: totalResult,
            class: TOTAL_CLASS_NAME
        });


        let yScale = d3.scaleLinear()
            .domain([0, maxValue * 1.1])
            .range([0, HEIGHT]);


        let xScale = d3.scaleBand()
            .domain(d3.range(0, data.length))
            .range([0, WIDTH])
            .paddingInner(0.1)
            .paddingOuter(0.2);

        function constructYAxis(selection, className, height, maxValue) {
            const yAxisScale = d3.scaleLinear()
                .domain([0, maxValue * 1.1])
                .range([height, 0]);
            const yAxis = d3.axisLeft(yAxisScale)
                .tickFormat(d3.format('.0p'));
            selection.attr('class', className)
                .call(yAxis);
        }

        function constructXAxis(selection, className, height, width, data) {
            const xAxisScale = d3.scaleBand()
                .domain(data.map((el, i) => el.label))
                .range([0, width])
                .paddingInner(0.1)
                .paddingOuter(0.2);

            const xAxis = d3.axisBottom(xAxisScale);
            selection
                .attr('class', className)
                .attr('transform', `translate(0, ${height})`)
                .call(xAxis);
        }

        svg.append('g')
            .call(constructXAxis, 'xAxis', HEIGHT, WIDTH, data);

        svg.append('g')
            .call(constructYAxis, 'yAxis', HEIGHT, maxValue);


        let t = d3.transition().duration(300).delay(100);


        function styleBars(selection) {
            selection.style('fill', (d) => {
                switch (d.class) {
                    case POSITIVE_CLASS_NAME:
                        return '#0fad6b';
                    case NEGATIVE_CLASS_NAME:
                        return '#ff5a5d';
                    case TOTAL_CLASS_NAME:
                        return '#2ab0ff';
                    default:
                        return;
                }
            })
        }

        function styleEmbedDataContainers(selection) {
            selection
                .style('stroke', 'none')
                .filter((d) => {
                    return d.embedData;
                })
                .style('stroke', 'rgb(111, 125, 122)')
                .style('stroke-width', 3)
                .style('stroke-dasharray', 15);
        }

        function tooltipAppearingBehaviour(selection) {
            selection.on('mouseover', function (d) {
                const x = d3.select(this).node().getBoundingClientRect().right - d3.select(this).node().getBoundingClientRect().width / 2;
                const y = d3.select(this).node().getBoundingClientRect().bottom - d3.select(this).node().getBoundingClientRect().height / 2;
                d3.select('#toolTip')
                    .style('opacity', '1')
                    .style('left', `${x}px`)
                    .style('top', `${y}px`)
                    .text(() => {
                        switch (d.class) {
                            case POSITIVE_CLASS_NAME:
                                return `+${d.value * 100}%`;
                            case NEGATIVE_CLASS_NAME:
                                return `-${d.value * 100}%`;
                            case TOTAL_CLASS_NAME:
                                return `${d.value * 100}%`;
                            default:
                                return;
                        }
                    });
            })
                .on('mouseout', () => {
                    d3.select('#toolTip')
                        .style('opacity', '0');
                })
        }

        let newData = data.map((el, i) => {
            obj = copyObject(el);
            obj.index = i;
            return obj;
        });
        let oldData = [];




        function expandBar(d, i) {
            if (d.embedData && d.embedData.length !== 0 && d3.select(this).selectAll('g').size() === 0) {

                newData.forEach((el, index) => {
                    el.index = index;
                });


                let globalIndex = d.index || i;

                d.embedData.forEach(el => {
                    el.animate = true
                });


                oldData.push(newData);

                newData = [
                    ...newData.slice(0, globalIndex),
                    ...d.embedData,
                    ...newData.slice(globalIndex + 1),
                ];

                newData.forEach((el, index) => {
                    el.index = index;
                });

                svg.select('.xAxis').call(constructXAxis, 'xAxis', HEIGHT, WIDTH, newData);

                xScale = d3.scaleBand()
                    .domain(d3.range(0, newData.length))
                    .range([0, WIDTH])
                    .paddingInner(0.1)
                    .paddingOuter(0.2);


                d3.select(this)
                    .remove();

                let selection = svg.selectAll('g.bar').data(newData, (d) => {
                    return d.label;
                });
                let enterSelection = selection.enter().append('g').attr('new', 'true').on('click', expandBar);

                enterSelection.append('rect');


                let merged = selection.merge(enterSelection);


                merged
                    .attr('class', (d) => {
                        return `bar ${d.class}`
                    })
                    .call(tooltipAppearingBehaviour);


                let mainSelection = merged
                    .filter((d) => {
                        return !d.animate;
                    });

                mainSelection
                    .select('rect')
                    .attr('y', (d) => {
                        switch (d.class) {
                            case POSITIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.end);
                            case NEGATIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.start);
                            case TOTAL_CLASS_NAME:
                                return HEIGHT - yScale(d.value);
                            default:
                                return;
                        }
                    })
                    .transition(t)
                    .attr('x', (d, i) => {
                        return xScale(d.index)
                    })
                    .attr('class', (d) => {
                        return d.class;
                    })
                    .attr('width', (d, i) => {
                        return xScale.bandwidth();
                    })
                    .attr('height', (d) => {
                        return Math.abs(yScale(d.start) - yScale(d.end))
                    })
                    .call(styleBars)
                    .call(styleEmbedDataContainers);


                let selectionToAnimate = merged
                    .filter((d) => {
                        return d.animate;
                    });

                selectionToAnimate
                    .select('rect')
                    .attr('y', d => {
                        switch (d.class) {
                            case POSITIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.end);
                            case NEGATIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.start);
                            case TOTAL_CLASS_NAME:
                                return HEIGHT - yScale(d.value);
                            default:
                                return;
                        }
                    })
                    .attr('x', (d) => {
                        return xScale(d.index)
                    })
                    .attr('class', (d) => {
                        return d.class;
                    })
                    .call(styleBars)
                    .call(styleEmbedDataContainers)
                    .attr('width', () => {
                        return xScale.bandwidth();
                    })
                    .transition(t)
                    .attr('height', (d) => {
                        return Math.abs(yScale(d.start) - yScale(d.end))
                    });

                d3.select('#toolTip')
                    .style('opacity', '0');

                newData.forEach(el => {
                    el.animate = false;
                })

            }
        }


        let bar = svg.selectAll('g.bar')
            .data(newData, (d) => {
                return d.label;
            })
            .enter()
            .append('g')
            .attr("class", (d) => {
                return `bar ${d.class}`
            })
            .on('click', expandBar);

//--------------------------------------------------------------------------------------------------------------------------//


        let button = d3.select('button');
        button.on('click', () => {
            if (oldData.length !== 0) {
                let stepData = oldData.pop();
                newData = stepData;
                svg.select('.xAxis').transition(t).call(constructXAxis, 'xAxis', HEIGHT, WIDTH, stepData);

                xScale = d3.scaleBand()
                    .domain(d3.range(0, stepData.length))
                    .range([0, WIDTH])
                    .paddingInner(0.1)
                    .paddingOuter(0.2);


                let selection = svg.selectAll('g.bar').data(stepData, (d) => {
                    return d.label
                });
                selection.exit().remove();

                let enterSelection = selection.enter().append('g').attr('class', (d) => {
                    return `bar ${d.class}`
                }).attr('new', 'yes').append('rect');


                selection.select('rect')
                    .attr('y', d => {
                        switch (d.class) {
                            case POSITIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.end);
                            case NEGATIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.start);
                            case TOTAL_CLASS_NAME:
                                return HEIGHT - yScale(d.value);
                            default:
                                return;
                        }
                    })
                    .call(styleBars)
                    .call(styleEmbedDataContainers)
                    .transition(t)
                    .attr('x', (d, i) => {
                        return xScale(i)
                    })
                    .attr('class', (d) => {
                        return d.class;
                    })
                    .attr('width', (d) => {
                        return xScale.bandwidth();
                    })
                    .attr('height', (d) => {
                        return Math.abs(yScale(d.start) - yScale(d.end))
                    });

                enterSelection
                    .attr('y', d => {
                        switch (d.class) {
                            case POSITIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.end);
                            case NEGATIVE_CLASS_NAME:
                                return HEIGHT - yScale(d.start);
                            case TOTAL_CLASS_NAME:
                                return HEIGHT - yScale(d.value);
                            default:
                                return;
                        }
                    })
                    .call(styleBars)
                    .call(styleEmbedDataContainers)
                    .attr('x', (d, i) => {
                        return xScale(i)
                    })
                    .attr('class', (d) => {
                        return d.class;
                    })
                    .attr('width', (d) => {
                        return xScale.bandwidth();
                    })
                    .on('click', expandBar)
                    .transition(t)
                    .attr('height', (d) => {
                        return Math.abs(yScale(d.start) - yScale(d.end))
                    });
            }
        });


// -------------------------------------------------------------------------------------------------------------------------//


        bar.append('rect')
            .attr('x', (d, i) => {
                return xScale(i)
            })
            .attr('y', (d) => {
                switch (d.class) {
                    case POSITIVE_CLASS_NAME:
                        return HEIGHT - yScale(d.end);
                    case NEGATIVE_CLASS_NAME:
                        return HEIGHT - yScale(d.start);
                    case TOTAL_CLASS_NAME:
                        return HEIGHT - yScale(d.value);
                    default:
                        return;
                }
            })
            .attr('class', (d) => {
                return d.class;
            })
            .attr('width', xScale.bandwidth())
            .attr('height', (d) => {
                return Math.abs(yScale(d.start) - yScale(d.end))
            })
            .call(styleBars)
            .call(styleEmbedDataContainers)
            .call(tooltipAppearingBehaviour);

    }
}(window));