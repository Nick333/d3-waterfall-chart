let randomDataSet = (count) => {
    let arr = [];
    for (let i = 0; i < count; i++) {
        arr.push(Math.random() * 100)
    }
    return arr;
};

let h = 600;
let w = 600;

let dataset = randomDataSet(70);

let svg = d3.select('#chartArea')
    .append('svg')
    .attr('height', h)
    .attr('width', w);

let yScale = d3.scaleLinear()
    .domain([0, d3.max(dataset) * 1.1])
    .range([0, h]);

let xScale = d3.scaleBand()
    .domain(d3.range(0, dataset.length))
    .range([0, w])
    .paddingInner(0.1);

let colorScale = d3.scaleLinear()
    .domain([0, dataset.length])
    .range(['green', 'red']);

svg.selectAll('rect')
    .data(dataset)
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', (d, i) => {
        return xScale(i);
    })
    .attr('y', (d) => {
        return h - yScale(d)
    })
    .attr('width', xScale.bandwidth())
    .attr('height', (d) => {
        return yScale(d);
    })
    .style('fill', (d, i) => {
        return colorScale(i);
    });
